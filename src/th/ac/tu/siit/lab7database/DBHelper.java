package th.ac.tu.siit.lab7database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
	
	private static final String DBNAME = "contacts.db";
	private static final int DBVERSION = 1;
	
	public DBHelper(Context ctx) {
		super(ctx, DBNAME, null, DBVERSION);
	}
	
	//Called when the application is newly installed
	//No database file in the internal storage.
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		// primary key of table must be "_id"
		// When we want to use thus table with ListView
		String sql = "CREATE TABLE contacts (" +
				"_id integer primary key autoincrement, " +
				"ct_name text not null, " +
				"ct_phone text not null, " +
				"ct_type integer default 0, " +
				"ct_email text not null);";
		db.execSQL(sql);		
	}
	// Called when the database file exists,
	// but DBVERSION was increased.
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Upgrade by removing current table, and recreate
		// This is NOT a practical way.
		// should ALTER TABLE when need to change structure of table
		String sql = "DROP TABLE IF EXISTS contacts;";
		db.execSQL(sql);
		this.onCreate(db);
	}

}
